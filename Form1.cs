using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace CryptoWrapper
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//QRNyF07meyA0ATXPs12DzDo/U6kg21AAYIF.FzI8B.bhUmf.w00EN0t6pJt.3eAqD.7wKVw.fwESx1gfL0L/Prv6E1DLXxs.ccNbQ.g6E860Cziy80.ZLj30
			string s = CryptoWrapper.DecryptString("SwtGGku0Q4nOC3DdsotMN6+Ii/XCpSemfBtOwGtweGkzAyb0HPoecpBaUEmw", "jEE2i/eQHvA0qpFlE/JjY.x1", null, 0);
			MessageBox.Show(s);
			CryptoWrapper.FreeResultString(s);
			//MessageBox.Show(CryptoWrapper.EncryptString("test", "Win32Mirc/DevCppMingW/Release", null, 1000));
			//MessageBox.Show(CryptoWrapper.DecryptString("test", "y6Xej.F22PR0TmS1q0GZ4OC14WU5p1HH0tn0g2W5n.HPXsC1", null, 0));
			//y6Xej.F22PR0TmS1q0GZ4OC14WU5p1HH0tn0g2W5n.HPXsC1
			//MessageBox.Show(CryptoWrapper.DecryptString("test", CryptoWrapper.EncryptString("test", "hej", null, 100), null, 100));
			
			//MessageBox.Show(CryptoWrapper.GetVersionString());
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Size = new System.Drawing.Size(300,300);
			this.Text = "Form1";
		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
	}
}
